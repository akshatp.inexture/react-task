import React, { useState } from "react";
import axios from "axios";

const Rhyme = () => {
  const [data, setData] = useState("");
  const [input, setInput] = useState("");

  // console.log(input);
  // console.log(data);

  //   functions
  const handleClick = () => {
    let getdata = async () => {
      let res = await axios.get(
        `https://api.datamuse.com/words?rel_rhy=${input}`
      );
      // console.log(res.data.articles);
      setData(res.data);

      if (res.data.length === 0) {
        return window.alert("No Match Found");
      }
    };
    getdata();
  };

  return (
    <div>
      <h3>Find Rhyme Word</h3>
      <input
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="Enter Word"
      />
      <button onClick={handleClick}>Search</button>

      <div>
        {data &&
          data.map((item, index) => (
            <div key={index}>
              <p>
                Rhyme word: <b> {item.word}</b>
              </p>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Rhyme;
