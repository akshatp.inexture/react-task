var server_echo;
var json = {
  json: JSON.stringify({
    a: 1,
    b: 2,
  }),
  delay: 3,
};
fetch("/echo/", {
  method: "post",
  headers: {
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/json",
  },
  // we have already converted json to string hence we don't need to convert it again in string.
  body:
    "json=" +
    encodeURIComponent(JSON.stringify(json.json)) +
    "&delay=" +
    json.delay,
})
  //   we can not write json() two time because this method is used for getting our json response from http response. if we write it two times it will give error of body stream already read.
  .then(function (response) {
    // we can not store our json response directly because this process is asynchronous.Hence,here it will give promise pending.
    server_echo = response.json().echo;
    return response.json();
  })
  .then(function (result) {
    alert(result);
  })
  .catch(function (error) {
    console.log("Request failed", error);
  });
server_echo.forEach((element) => console.log(element));
