import "./App.css";
import Rhyme from "./Task/Rhyme";

function App() {
  return (
    <div className="App">
      <Rhyme />
    </div>
  );
}

export default App;
